namespace Core.Models.DTO.Account
{
    public class LoginAccountModel
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
}