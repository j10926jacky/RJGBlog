namespace Core.Models.DTO.Account
{
    public class RegisterAccountModel
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}