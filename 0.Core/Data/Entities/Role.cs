﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Data.Entities
{
    public class Role
    {
        public Role()
        {
            RoleMenu = new HashSet<RoleMenu>();
            RoleUser = new HashSet<RoleUser>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string Name { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreateTime { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdateTime { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<RoleMenu> RoleMenu { get; set; }
        [InverseProperty("Role")]
        public virtual ICollection<RoleUser> RoleUser { get; set; }
    }
}