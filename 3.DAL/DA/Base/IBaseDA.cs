﻿namespace DAL.DA.Base
{
    public interface IBaseDA
    {
        int SaveChanges();
    }
}